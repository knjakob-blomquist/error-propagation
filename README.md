You can download latest version of error propagation by the following commands:
git clone https://gitlab.com/knjakob-blomquist/error-propagation.git

It is also possible to download it directly from this page by using the download feature of GitLab.

Included in the package is the interactive jupyter notebook file error_propagation.ipynb
 and error_propagation.html, which is almost identical but non-interactive version.

As it is a jupyter notebook, for the interactive file (error_propagation.ipynb) to work, you need to have python3 and jupyter notebook installed.
See: https://jupyter.org/index.html 
To be able to run all examples you also need to have matplotlib, numpy, scipy, seaborn, pandas and lmfit installed.
The recommended way to install many of these is though PiPy or PIP: https://www.liquidweb.com/kb/install-pip-windows/ 
If you don't already have it I would recommend that you install pipy and after that it should just be a matter of typing in the command prompt or terminal: 
   pip3 install --upgrade --user lmfit
To install lmfit. Most times PIP will handle any dependancies so the user should not have to worry. 
Another way to install most of the needed python packages is by installing Anaconda https://www.anaconda.com/downloads
